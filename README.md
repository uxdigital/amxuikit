# UI Kit API v2.02

https://bitbucket.org/uxdigital/amxuikit

Control Designs Software Ltd

UI Kit provides an advanced function based means of controlling an AMX user interface. Now out of Beta, but being updated all the time, it lets an AMX programmer call things like button commands and page / popup commands easily using a library of calls. Documentation will improve as this library goes on.

To use this in your project you can include it as a submodule if already using git. To do this use the following command in order to add the submodule in a folder called `/ui-kit` folder of your repository root: 

    git submodule add https://bitbucket.org/uxdigital/amxuikit.git ui-kit

You will now have a folder in your project called `/ui-kit` and you will need to add the included `.axi` files into your AMX project workspace.

## Revision History
* * *
#### v2.1

- NEW! Added button events

#### v2.02

- Updated this README
- Some Documentation ... more to follow asap!

#### v2.01

- Bug Fix: UIPageNameFromID() number of arguments changed to reflect only those needed
- Bug Fix: Fixed a problem with last commit
- Bug Fix: Fixed argument declaration for UIPasswordEnterCharacter() function
- Added background popup name for UIActionSheet() so you can create modal style animation popups
- New functions and data storage for setting an int val on the ui for a nav mode (could be a popup index / mode) on an individual page id
- Changes to UIPassword and new functions for messages and length
- UIWaitStart() pageName now is not required if Page flip isn't wanted

#### v2.0

- First official release
- UIButtonHold function names changed

## API Guide
* * *
### Callback Functions (required)

The use of this API requires several functions to be included in your code. These are called by the API includes to assist setup various elements such as keys for devices and variable storage.

#### UserInterfacesShouldRegister

Here the API will call this function for you to register the device using **UIRegisterDevice**

    DEFINE_FUNCTION UserInterfacesShouldRegister() {
        // Register your UI device
	    UIRegisterDevice(deviceKey, deviceName, groupKey, device);
    }

#### UserInterfaceVarsShouldRegister

The API calls this after registration in order for you to initialise and register any variables you may want to store with **UIVarRegister**

    DEFINE_FUNCTION UserInterfaceVarsShouldRegister() {
	    // Register any variables used for the device
	    UIVarRegister(groupKey|deviceKey, varKey, defaultValue);
    }

#### UserInterfaceHasRegistered

Called after the device has been registered and is ready to be used

    DEFINE_FUNCTION UserInterfaceHasRegistered(CHAR uiDeviceKey[]) {
	    // Anything you may want to call after the device has been reistered
    }

#### UIButtonEvent

    DEFINE_FUNCTION UIButtonEvent(CHAR uiDeviceKey[], CHAR btnKey[], INTEGER eventType) {
        switch(eventType) {
            case UI_BTN_EVENT_TYPE_TAP: {
                // Single tap and release button events
                
            }
            case UI_BTN_EVENT_TYPE_HOLD: {
                // Hold events
                
            }
        }
    }

### Register Device Functions

#### UIRegisterDevice

Use to register the UI device with the API

    UIRegisterDevice(deviceKey, deviceName, groupKey, device);

**Arguments**

- **deviceKey** *CHAR[]* Unique key for your device
- **deviceName** *CHAR[]* Descriptive name
- **groupKey** *CHAR[]* A key which can be used to contain a group of devices
- **device** *DEV* AMX Device address structure

#### UIVarRegister

Use to register any variable you may want to track with this instance of device

    UIVarRegister(groupKey|deviceKey, varKey, defaultValue);

**Arguments**

- **groupKey|deviceKey** *CHAR[]* Unique key for your device or the associated group key
- **varKey** *CHAR[]* Unique key for the variable
- **defaultValue** *CHAR[]* Initial value of the variable string

## License
* * *
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

- The above copyright notice and this permission notice and header shall be included in all copies or substantial portions of the Software.

- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.